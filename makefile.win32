# See http://www.geany.org/Support/BuildingOnWin32
# Running make creates config.h then calls the sub makefiles.
#
# Other targets are:
#	deps: delete the dependencies so they are regenerated on next make
#	clean: clean all generated files
#	install: install to DESTDIR
#
# localwin32.mk is an optional file to override make variables.
# Use localwin32.mk instead of editing variables as it is included in sub
# makefiles.
# E.g. use localwin32.mk to set PREFIX=C:/libs instead of the default C:\libs
# For MSYS use localwin32.mk to set CP, CP_R, RM, DIRSEP.
# By default this should work in a Windows command prompt (cmd.exe).

WINDRES = windres.exe
CC = gcc
CXX = g++
CP = copy /Y
CP_R = xcopy /S /Y
RM = del
MKDIR = mkdir
# strip is used to prevent line wrap
DIRSEP := $(strip \)
DESTDIR = C:/Program Files/Geany
-include localwin32.mk

all: config.h
	$(MAKE) -C tagmanager/mio -f makefile.win32
	$(MAKE) -C tagmanager -f makefile.win32
	$(MAKE) -C scintilla -f makefile.win32
	$(MAKE) -C plugins -f makefile.win32
	$(MAKE) -C src -f makefile.win32

config.h: win32-config.h
	$(CP) $< $@

deps:
	-$(RM) tagmanager/mio/deps.mak tagmanager/deps.mak scintilla/deps.mak plugins/deps.mak src/deps.mak

# used by src/makefile.win32 to avoid del ../file which is an error
clean-local:
	-$(RM) geany_private.res geany.exe

clean: deps
	$(MAKE) -C tagmanager/mio -f makefile.win32 clean
	$(MAKE) -C tagmanager -f makefile.win32 clean
	$(MAKE) -C scintilla -f makefile.win32 clean
	$(MAKE) -C plugins -f makefile.win32 clean
	$(MAKE) -C src -f makefile.win32 clean

.PHONY: install

# likely requires admin privileges
# mkdir output is ignored in case dir exists
# 'copy' seems to only accept / in the destination
install:
	-$(MKDIR) "$(DESTDIR)"
	-$(MKDIR) "$(DESTDIR)/bin"
	$(CP) geany.exe "$(DESTDIR)/bin"
	-$(MKDIR) "$(DESTDIR)/lib"
	$(CP) plugins$(DIRSEP)*.dll "$(DESTDIR)/lib"
	-$(MKDIR) "$(DESTDIR)/data"
	$(CP_R) data "$(DESTDIR)/data"
